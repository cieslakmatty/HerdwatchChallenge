//https://docs.angularjs.org/api/
//https://material.angularjs.org/

var app = angular.module('myModule', ['ngAnimate', 'ngMaterial', 'ngMessages'])

.controller("myController", function($scope, $mdSidenav){
    $scope.options = ["screen 0", "screen 1", "screen 2", "screen 3", "screen 4", "screen 5", "screen 6", "screen 7", "screen 8"];
    
    $scope.toggleLeft = function(){
        $mdSidenav('left').toggle();
    }

    $scope.openLeft = function(){
        $mdSidenav('left').open();
    }

    $scope.closeLeft = function(){
        $mdSidenav('left').close();
    }

    $scope.isOpenLeft = function(){
      return $mdSidenav('left').isOpen();
    };

    $scope.setParaElement = function(option){
        angular.element(document.querySelector('#optionsDisplay'))[0].innerHTML = option;
    }
})

.controller('LeftController', function ($scope, $mdSidenav) {
    $scope.close = function () {
        $mdSidenav('left').close();
    };
  })