//https://docs.angularjs.org/api/
//https://material.angularjs.org/

var app = angular.module('myModule', ['ngAnimate', 'ngMaterial', 'ngMessages'])

.controller("myController", function($scope, $mdSidenav){
    $scope.options = ["screen 0", "screen 1", "screen 2", "screen 3", "screen 4", "screen 5", "screen 6", "screen 7", "screen 8"];
    
    $scope.toggleRight = function(){
        $mdSidenav('right').toggle();
    }

    $scope.isOpenRight = function(){
      return $mdSidenav('right').isOpen();
    };

    $scope.setParaElement = function(option){
        angular.element(document.querySelector('#optionsDisplay'))[0].innerHTML = option;
    }
})

.controller('RightController', function ($scope, $mdSidenav) {
    $scope.close = function () {
        $mdSidenav('right').close();
    };
  })